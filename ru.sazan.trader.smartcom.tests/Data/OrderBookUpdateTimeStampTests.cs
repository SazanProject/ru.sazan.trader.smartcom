﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class OrderBookUpdateTimeStampTests
    {        
        [TestMethod]
        public void OrderBookUpdateTimeStamp_is_singleton_test()
        {
            OrderBookLastUpdateTimeStamped first = OrderBookUpdateTimeStamp.Instance;
            OrderBookLastUpdateTimeStamped second = OrderBookUpdateTimeStamp.Instance;
            
            Assert.AreSame(first, second);
            Assert.AreEqual(DateTime.MinValue, first.DateTime);
            Assert.AreEqual(DateTime.MinValue, second.DateTime);
        }
    }
}
