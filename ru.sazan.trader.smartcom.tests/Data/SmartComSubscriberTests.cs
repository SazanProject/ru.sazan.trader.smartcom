﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.smartcom.tests.Mocks;
using ru.sazan.trader.smartcom.Data;
using SmartCOM3Lib;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class SmartComSubscriberTests
    {
        private SmartComSubscriber subscriber;

        [TestInitialize]
        public void Setup()
        {
            this.subscriber = new SmartComSubscriber(new StServerClassMock(), new NullLogger());
        }

        [TestMethod]
        public void Subscriber_Increments_SubscribtionsCounter_After_Subscribe()
        {
            this.subscriber.Ticks.Add("RTS-6.13_FT");
            this.subscriber.Portfolios.Add("ST88888-RF-01");
            this.subscriber.BidsAndAsks.Add("Si-6.13_FT");

            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);

            this.subscriber.Subscribe();

            Assert.AreEqual(3, this.subscriber.SubscriptionsCounter);
        }

        [TestMethod]
        public void Subscriber_Decrements_SubscribtionsCounter_After_Unsubscribe()
        {
            this.subscriber.Ticks.Add("RTS-6.13_FT");
            this.subscriber.Portfolios.Add("ST88888-RF-01");
            this.subscriber.BidsAndAsks.Add("Si-6.13_FT");

            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);

            this.subscriber.Subscribe();

            Assert.AreEqual(3, this.subscriber.SubscriptionsCounter);

            this.subscriber.Unsubscribe();

            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);
        }

        [TestMethod]
        public void Subscriber_Do_Nothing_For_Empty_Subscriptions_Lists()
        {
            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);

            this.subscriber.Subscribe();

            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);
        }

        [TestMethod]
        public void Subscriber_Do_Nothing_For_Empty_Subscriptions_After_Update()
        {
            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);

            this.subscriber.Subscribe();

            Assert.AreEqual(0, this.subscriber.SubscriptionsCounter);
        }

        [TestMethod]
        public void DefaultSubscriber_IsSingleton()
        {
            SmartComSubscriber s = DefaultSubscriber.Instance;
            SmartComSubscriber s1 = DefaultSubscriber.Instance;

            Assert.AreSame(s, s1);
        }
    }
}
