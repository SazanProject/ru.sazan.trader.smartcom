﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.smartcom.Handlers;

namespace ru.sazan.trader.smartcom.tests.Handlers
{
    [TestClass]
    public class MakeOrderDeliveryConfirmationOnOrderSucceededTests
    {
        private BaseDataContext rawData;
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.rawData = new RawTradingDataContext();
            this.tradingData = new TradingDataContext();

            MakeOrderDeliveryConfirmationOnOrderSucceeded handler =
                new MakeOrderDeliveryConfirmationOnOrderSucceeded(this.rawData, this.tradingData, new NullLogger());
        }

        [TestMethod]
        public void SetDeliveryDate_test()
        {
            Strategy strategy = new Strategy(1, "Sample strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal, 150);
            this.tradingData.Get<ICollection<Order>>().Add(order);
            Assert.IsFalse(order.IsDelivered);
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<OrderDeliveryConfirmation>>().Count());
            
            OrderSucceeded os = new OrderSucceeded(order.Id, "12345");
            this.rawData.GetData<OrderSucceeded>().Add(os);

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<OrderDeliveryConfirmation>>().Count());
            OrderDeliveryConfirmation confirmation = this.tradingData.Get<IEnumerable<OrderDeliveryConfirmation>>().Last();
            Assert.AreEqual(order.Id, confirmation.OrderId);
            Assert.AreEqual(order, confirmation.Order);
            Assert.AreEqual(os.DateTime, confirmation.DateTime);

        }

        [TestMethod]
        public void IgnoreDeliveryDate_test()
        {
            Strategy strategy = new Strategy(1, "Sample strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal, 150);
            this.tradingData.Get<ICollection<Order>>().Add(order);
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<OrderDeliveryConfirmation>>().Count());

            OrderSucceeded os = new OrderSucceeded(5, "12345");
            this.rawData.GetData<OrderSucceeded>().Add(os);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<OrderDeliveryConfirmation>>().Count());
        }
    }
}
