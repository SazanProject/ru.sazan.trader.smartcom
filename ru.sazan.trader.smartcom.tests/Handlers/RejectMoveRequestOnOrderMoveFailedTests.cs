﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.smartcom.Handlers;

namespace ru.sazan.trader.smartcom.tests.Handlers
{
    [TestClass]
    public class RejectMoveRequestOnOrderMoveFailedTests
    {
        private DataContext tradingData;
        private RawTradingDataContext rawData;
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.rawData = new RawTradingDataContext();
            
            this.strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-12.13_FT", 1);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);

            RejectMoveRequestOnOrderMoveFailed handler =
                new RejectMoveRequestOnOrderMoveFailed(this.tradingData, this.rawData, new NullLogger());
        }

        [TestMethod]
        public void RejectMoveRequest_on_OrderMoveFailed_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal);
            this.tradingData.Get<ICollection<Order>>().Add(order);

            OrderMoveRequest request = new OrderMoveRequest(order, 151000, 0, "Move order");
            this.tradingData.Get<ICollection<OrderMoveRequest>>().Add(request);

            Assert.IsFalse(request.IsFailed);

            OrderMoveFailed fault = new OrderMoveFailed(order.Id, "268", "Reason");
            this.rawData.GetData<OrderMoveFailed>().Add(fault);

            Assert.IsTrue(request.IsFailed);
        }

        [TestMethod]
        public void ignore_duplicated_OrderMoveFailed_test()
        {
            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order order = new Order(signal);
            this.tradingData.Get<ICollection<Order>>().Add(order);

            OrderMoveRequest request = new OrderMoveRequest(order, 151000, 0, "Move order");
            this.tradingData.Get<ICollection<OrderMoveRequest>>().Add(request);

            Assert.IsFalse(request.IsFailed);

            OrderMoveFailed fault = new OrderMoveFailed(order.Id, "268", "Reason");
            this.rawData.GetData<OrderMoveFailed>().Add(fault);

            Assert.IsTrue(request.IsFailed);

            OrderMoveFailed duplicate = new OrderMoveFailed(order.Id, "268", "Reason duplicate");
            this.rawData.GetData<OrderMoveFailed>().Add(duplicate);

            Assert.AreEqual(request.FaultDescription, fault.Reason);
            Assert.AreNotEqual(request.FaultDescription, duplicate.Reason);
        }

    }
}
