﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Events;
using ru.sazan.trader.smartcom.Net;

namespace ru.sazan.trader.smartcom.tests.Mocks
{
    public class FakeAdapterBase
    {
        private SmartComHandlersDatabase handlers;
        public SmartComHandlersDatabase Handlers
        {
            get
            {
                return this.handlers;
            }
        }

        private SmartComBinder binder;
        public SmartComBinder Binder
        {
            get
            {
                return this.binder;
            }
        }

        private StServerMockSingleton mockSingleton;
        public StServerMockSingleton StServerMockSingleton
        {
            get
            {
                return this.mockSingleton;
            }
        }

        private SmartComSubscriber subscriber;
        public SmartComSubscriber Subscriber
        {
            get
            {
                return this.subscriber;
            }
        }

        private SmartComConnector connector;
        public SmartComConnector Connector
        {
            get
            {
                return this.connector;
            }
        }

        public FakeAdapterBase()
        {
            this.handlers = new SmartComHandlersDatabase();
            this.mockSingleton = new StServerMockSingleton();
            this.subscriber = new SmartComSubscriber(this.mockSingleton.Instance, new NullLogger());
            this.binder = new SmartComBinder(this.mockSingleton.Instance, this.handlers, new NullLogger());
            this.connector = new SmartComConnector(this.mockSingleton.Instance, this.handlers, new NullLogger());
        }
    }
}
