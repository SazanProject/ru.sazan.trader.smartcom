﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using SmartCOM3Lib;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Commands
{
    public class GetSymbolsCommand:Transaction
    {
        private GenericSingleton<StServer> singleton;
        private Logger logger;

        public GetSymbolsCommand() :
            this(new StServerSingleton(), DefaultLogger.Instance) { }

        public GetSymbolsCommand(GenericSingleton<StServer> singleton, Logger logger)
        {
            this.singleton = singleton;
            this.logger = logger;
        }

        public void Execute()
        {
            this.singleton.Instance.GetSymbols();

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправлен запрос на получение информации о торгуемых инструментах.", 
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name));
        }
    }
}
