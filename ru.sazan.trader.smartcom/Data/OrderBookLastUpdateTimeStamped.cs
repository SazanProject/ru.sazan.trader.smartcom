﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Data
{
    public class OrderBookLastUpdateTimeStamped : TimeStamped
    {
        private SymbolDataHasBeenUpdatedNotifier notifier;

        public OrderBookLastUpdateTimeStamped(SymbolDataHasBeenUpdatedNotifier notifier)
        {
            this.notifier = notifier;
            this.lastNotification = DateTime.MinValue;
            this.notifier.OnQuotesUpdate += UpdateLastNotificationOnQuotesUpdate;
        }

        private void UpdateLastNotificationOnQuotesUpdate(string symbol)
        {
            this.lastNotification = BrokerDateTime.Make(DateTime.Now);
        }

        private DateTime lastNotification;
        public DateTime DateTime
        {
            get { return this.lastNotification; }
        }
    }
}
