﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Data
{
    public class OrderBookUpdateTimeStamp : OrderBookLastUpdateTimeStamped
    {
        private static OrderBookUpdateTimeStamp instance = null;

        public static OrderBookUpdateTimeStamp Instance
        {
            get
            {
                if (instance == null)
                    instance = new OrderBookUpdateTimeStamp();

                return instance;
            }
        }

        private OrderBookUpdateTimeStamp() :
            base(OrderBook.Instance) { }
    }
}
