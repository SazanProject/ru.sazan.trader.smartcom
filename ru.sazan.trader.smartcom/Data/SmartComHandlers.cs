﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.Data
{
    public class SmartComHandlers:SmartComHandlersDatabase
    {
        private static SmartComHandlers theInstance = null;

        private SmartComHandlers() { }

        public static SmartComHandlers Instance
        {
            get
            {
                if (theInstance == null)
                    theInstance = new SmartComHandlers();

                return theInstance;
            }
        }
    }
}
