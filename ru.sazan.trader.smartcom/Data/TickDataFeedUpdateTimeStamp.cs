﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Data
{
    public class TickDataFeedUpdateTimeStamp : ItemAddedLastTimeStamped<Tick>
    {
        private static TickDataFeedUpdateTimeStamp instance = null;

        public static TickDataFeedUpdateTimeStamp Instance
        {
            get
            {
                if (instance == null)
                    instance = new TickDataFeedUpdateTimeStamp();

                return instance;
            }
        }

        private TickDataFeedUpdateTimeStamp():
            base(TradingData.Instance.Get<ItemHasBeenAddedNotifier<Tick>>()) { }
    }
}
