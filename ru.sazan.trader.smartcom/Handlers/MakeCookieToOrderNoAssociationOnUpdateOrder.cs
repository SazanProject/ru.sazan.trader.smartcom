﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Utility;
using SmartCOM3Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class MakeCookieToOrderNoAssociationOnUpdateOrder : GenericCollectionObserver<UpdateOrder>
    {
        private Logger logger;

        public MakeCookieToOrderNoAssociationOnUpdateOrder() :
            this(RawTradingData.Instance,
             DefaultLogger.Instance) { }

        public MakeCookieToOrderNoAssociationOnUpdateOrder(BaseDataContext rawData, Logger logger)
            :base(rawData)
        {
            this.logger = logger;
        }

        public override void Update(UpdateOrder item)
        {
            if (item.Cookie == 0)
                return;

            if (item.OrderNo == "0")
                return;

            if(!item.CanContainFillingMark())
                return;

            if (DuplicateExistsFor(item))
                return;

            RegisterTradeInfoPending(item);
        }

        private void RegisterTradeInfoPending(UpdateOrder item)
        {
            CookieToOrderNoAssociation expectedTradeInfo = new CookieToOrderNoAssociation(item.Cookie, item.OrderNo);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, {2}",
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name,
                expectedTradeInfo.ToString()));

            this.dataContext.GetData<CookieToOrderNoAssociation>().Add(expectedTradeInfo);
        }

        private bool DuplicateExistsFor(UpdateOrder item)
        {
            try
            {
                return this.dataContext.GetData<UpdateOrder>().Where(i => i.Cookie == item.Cookie
                    && i.OrderId == item.OrderId
                    && i.OrderNo == item.OrderNo
                    && i.OrderUnfilled == item.OrderUnfilled).Count() > 1;
            }
            catch
            {
                return false;
            }
        }

    }
}
