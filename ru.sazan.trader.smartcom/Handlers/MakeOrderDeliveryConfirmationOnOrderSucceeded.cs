﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class MakeOrderDeliveryConfirmationOnOrderSucceeded:GenericCollectionObserver<OrderSucceeded>
    {
        private DataContext tradingData;
        private Logger logger;

        public MakeOrderDeliveryConfirmationOnOrderSucceeded()
            : this(RawTradingData.Instance, TradingData.Instance, DefaultLogger.Instance) { }

        public MakeOrderDeliveryConfirmationOnOrderSucceeded(BaseDataContext rawData, DataContext tradingData, Logger logger)
            :base(rawData)
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void Update(OrderSucceeded item)
        {
            Order order = GetOrder(item.Cookie);

            if (order == null)
                return;

            OrderDeliveryConfirmation confirmation = new OrderDeliveryConfirmation(order, item.DateTime);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сформировано уведомление о доставке заявки {2}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                confirmation.ToString()));

            this.tradingData.Get<ObservableHashSet<OrderDeliveryConfirmation>>().Add(confirmation);
        }

        private Order GetOrder(int orderId)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().Single(o => o.Id == orderId);
            }
            catch
            {
                return null;
            }
        }
    }
}
