﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class MakePendingTradeInfoOnTradeInfo:GenericCollectionObserver<TradeInfo>
    {
        private Logger logger;

        public MakePendingTradeInfoOnTradeInfo()
            : this(RawTradingData.Instance, DefaultLogger.Instance) { }

        public MakePendingTradeInfoOnTradeInfo(BaseDataContext rawData, Logger logger)
            :base(rawData)
        {
            this.logger = logger;
        }

        public override void Update(TradeInfo item)
        {
            if (HasDuplicate(item))
                return;

            PendingTradeInfo expectedUpdateOrder =
                new PendingTradeInfo(item);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, {2}",
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name,
                expectedUpdateOrder.ToString()));

            this.dataContext.GetData<PendingTradeInfo>().Add(expectedUpdateOrder);
        }

        private bool HasDuplicate(TradeInfo item)
        {
            try
            {
                return this.dataContext.GetData<TradeInfo>()
                    .Where(i => i.TradeNo.Equals(item.TradeNo))
                    .Count() > 1;
            }
            catch
            {
                return false;
            }
        }
    }
}
