﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Data;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class RejectOrderOnOrderFailed:GenericCollectionObserver<OrderFailed>
    {
        private DataContext tradingData;
        private Logger logger;

        public RejectOrderOnOrderFailed()
            : this(TradingData.Instance, RawTradingData.Instance, DefaultLogger.Instance) { }

        public RejectOrderOnOrderFailed(DataContext tradingData, BaseDataContext rawTradingData, Logger logger)
            :base(rawTradingData)
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void Update(OrderFailed item)
        {
            if (IsDuplicateOrderFailed(item))
                return;

            Order order = FindOrder(item.Cookie);

            if (order == null)
                return;

            order.Reject(BrokerDateTime.Make(DateTime.Now), item.Reason);
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, заявка отклонена {2}.", 
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name, 
                order.ToString()));
        }

        private bool IsDuplicateOrderFailed(OrderFailed item)
        {
            try
            {
                return this.dataContext.GetData<OrderFailed>().Where(u => u.OrderId == item.OrderId && u.Cookie == item.Cookie).Count() > 1;
            }
            catch
            {
                return false;
            }
        }

        private Order FindOrder(int id)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().Single(o => o.Id == id);
            }
            catch
            {
                return null;
            }
        }
    }
}
