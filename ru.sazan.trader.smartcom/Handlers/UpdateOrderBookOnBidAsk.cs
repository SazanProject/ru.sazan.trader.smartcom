﻿using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class UpdateOrderBookOnBidAsk : AddedItemHandler<BidAsk>
    {
        private OrderBookContext storage;
        private Logger logger;

        public UpdateOrderBookOnBidAsk()
            : this(TradingData.Instance) { }

        public UpdateOrderBookOnBidAsk(DataContext context)
            : this(OrderBook.Instance, context, DefaultLogger.Instance)
        { }

        public UpdateOrderBookOnBidAsk(OrderBookContext storage, DataContext context, Logger logger)
            : base(context.Get<ObservableCollection<BidAsk>>())
        {
            this.storage = storage;
            this.logger = logger;
        }

        public override void OnItemAdded(BidAsk item)
        {
            storage.Update(item.Row, item.Symbol, item.Bid, item.BidSize, item.Ask, item.AskSize);
        }
    }
}
