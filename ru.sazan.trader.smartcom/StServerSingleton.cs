﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCOM3Lib;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom
{
    public class StServerSingleton:GenericSingleton<StServer>
    {
        public StServer Instance
        {
            get { return SmartCom.Instance; }
        }

        public void Destroy()
        {
            SmartCom.Destroy();
        }

        public bool IsNull
        {
            get { return SmartCom.IsNull; }
        }
    }
}
